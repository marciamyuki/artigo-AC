
all: bib tex
	pdflatex template.tex
	evince template.pdf &

bib: template.bib
	pdflatex template.tex
	bibtex template

tex: template.tex
	pdflatex template.tex

clean:
	rm *.bbl *.aux *.blg *.log *.pdf
